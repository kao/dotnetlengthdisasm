﻿/******************************************************************************
*                                                                             *
* As simple as possible .NET length disassembler.                             *
* (c) 2018, kao. https://lifeinhex.com/                                       *
*                                                                             *
*                                                                             *
* "Perfection is achieved, not when there is nothing more to add, but when    *
* there is nothing left to take away" /Antoine de Saint-Exupery/              *
*                                                                             *
******************************************************************************/

using System;

namespace FixUnity3D
{
    static class DotnetLengthDisasm
    {
        const int IL_INVAL = -1;
        const int IL_SWTCH = -2;
        const int IL_OPFE = -3;

        static readonly int[] parameterSize = new int[] {
            0,        0,        0,        0,        0,        0,        0,        0,        // 00..07
            0,        0,        0,        0,        0,        0,        1,        1,        // 08..0F
            1,        1,        1,        1,        0,        0,        0,        0,        // 10..17
            0,        0,        0,        0,        0,        0,        0,        1,        // 18..1F
            4,        8,        4,        8,        IL_INVAL, 0,        0,        4,        // 20..27
            4,        4,        0,        1,        1,        1,        1,        1,        // 28..2F
            1,        1,        1,        1,        1,        1,        1,        1,        // 30..37
            4,        4,        4,        4,        4,        4,        4,        4,        // 38..3F
            4,        4,        4,        4,        4,        IL_SWTCH, 0,        0,        // 40..47
            0,        0,        0,        0,        0,        0,        0,        0,        // 48..4F
            0,        0,        0,        0,        0,        0,        0,        0,        // 50..57
            0,        0,        0,        0,        0,        0,        0,        0,        // 58..5F
            0,        0,        0,        0,        0,        0,        0,        0,        // 60..67
            0,        0,        0,        0,        0,        0,        0,        4,        // 68..6F
            4,        4,        4,        4,        4,        4,        0,        IL_INVAL, // 70..77
            IL_INVAL, 4,        0,        4,        4,        4,        4,        4,        // 78..7F
            4,        4,        0,        0,        0,        0,        0,        0,        // 80..87
            0,        0,        0,        0,        4,        4,        0,        4,        // 88..8F
            0,        0,        0,        0,        0,        0,        0,        0,        // 90..97
            0,        0,        0,        0,        0,        0,        0,        0,        // 98..9F
            0,        0,        0,        4,        4,        4,        IL_INVAL, IL_INVAL, // A0..A7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // A8..AF
            IL_INVAL, IL_INVAL, IL_INVAL, 0,        0,        0,        0,        0,        // B0..B7
            0,        0,        0,        IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // B8..BF
            IL_INVAL, IL_INVAL, 4,        0,        IL_INVAL, IL_INVAL, 4,        IL_INVAL, // C0..C7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // C8..CF
            4,        0,        0,        0,        0,        0,        0,        0,        // D0..D7
            0,        0,        0,        0,        0,        4,        1,        0,        // D8..DF
            0,        IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // E0..E7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // E8..EF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // F0..F7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL,  IL_OPFE, IL_INVAL  // F8..FF
        };

        static readonly int[] opFEparamSize = new int[] {
            0,        0,        0,        0,        0,        0,        4,        4,        // 00..07
            IL_INVAL, 2,        2,        2,        2,        2,        2,        0,        // 08..0F
            IL_INVAL, 0,        1,        0,        0,        4,        4,        0,        // 10..17
            0,        1,        0,        IL_INVAL, 4,        0,        0,        IL_INVAL, // 18..1F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 20..27
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 28..2F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 30..37
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 38..3F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 40..47
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 48..4F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 50..57
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 58..5F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 60..67
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 68..6F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 70..77
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 78..7F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 80..87
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 88..8F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 90..97
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // 98..9F
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // A0..A7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // A8..AF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // B0..B7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // B8..BF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // C0..C7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // C8..CF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // D0..D7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // D8..DF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // E0..E7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // E8..EF
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, // F0..F7
            IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL, IL_INVAL  // F8..FF
        };

        public static int GetInstructionSize(byte[] data, int index)
        {
            byte opcode = data[index];
            int instrSize = parameterSize[opcode];
            switch (instrSize)
            {
                case IL_INVAL:
                    throw new Exception(String.Format("Invalid opcode {0:X2}", opcode));
                case IL_SWTCH:
                    int caseCount = BitConverter.ToInt32(data, index + 1);
                    instrSize = 1 + 4 + caseCount * 4;
                    break;
                case IL_OPFE:
                    byte opFE = data[index + 1];
                    instrSize = opFEparamSize[opFE];
                    if (instrSize == IL_INVAL)
                        throw new Exception(String.Format("Invalid opcode FE {0:X2}", opFE));
                    else
                        instrSize += 2;
                    break;
                default:
                    instrSize += 1;
                    break;
            }
            return instrSize;
        }
    }
}
