# .NET Length Disasm #

This is a minimalistic .NET length disassembler. It could be used in .NET packers, unpackers and other low-level tools. 

### How to use ###

DotnetLengthDisasm exports just one function. You pass a byte[] and index to it, it returns length of the CIL instruction or throws an exception. As simple as that.
